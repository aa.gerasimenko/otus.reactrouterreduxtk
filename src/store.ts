import { configureStore } from "@reduxjs/toolkit";
import orderReducer from "./stateManagement/orderReducer";
import thunkReducer from "./stateManagement/thunkReducer";
import userReducer from "./stateManagement/userReducer";
import { combineReducers, createStore } from "redux";

// redux
const rootReducer = combineReducers({
  order: orderReducer,
  user: userReducer,
});

const store = createStore(rootReducer);

/*
RTK
const store = configureStore({
  reducer: {
    // Регистрируем два редюсера
    // Под порядковый номер
    order: orderReducer,
    // И под выбранного пользователя
    user: userReducer,
    // И под thunk
    funky: thunkReducer,
  },
});*/
export default store;
