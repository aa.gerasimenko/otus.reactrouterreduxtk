import { useParams, useSearchParams } from "react-router-dom";

export function Page2S() {
    const qp = useParams();

    const [queryParams, setQueryParams] = useSearchParams();

    const css = {
        margin: '10px',
        border: '5px solid red'
    };

    //http://localhost:3000/page2/fdjlksdfgdskl/qqqqqqq?query=123
    console.log(qp);
    console.log(queryParams);

    return <div style={css}>Это страница 2 с параметром {qp.id}  {JSON.stringify(queryParams.get('query'))}</div>
};

