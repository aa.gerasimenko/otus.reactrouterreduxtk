import { ConfigureStoreOptions } from "@reduxjs/toolkit";
import { useSelector } from "react-redux";
import { OrderThing } from "../../../models/orderThing";
import OrderItem from "./Item";

// interface Props {
// }

const CurrentLister = () => {
    // при помощи useSelector можем получить текущий стейт
    const orderedList = useSelector((x: {
        order: any
    }) => x.order.list);

    return <>
        {orderedList.map((x: OrderThing) => {
            return <div style={{ display: 'inline' }}>
                <OrderItem item={x} display="inline" />
            </div>
        })}
    </>;
};

export default CurrentLister;