import React from 'react';
import logo from './logo.svg';
import './App.css';
import {
  Route,
  Link,
  Routes,
  BrowserRouter
} from "react-router-dom";
import Page1 from './components/pages/Page 1/Page1';
import Page2 from './components/pages/Page 2/Page2';
import Home from './components/pages/Home/Home';
// import OrdererMain from './components/pages/Orderer';
import { Page2S } from './components/pages/Page 2/Page2Specific';
import ApeList from './components/pages/Ape/ApeList';
import Ape300 from './components/pages/Ape/Ape';
// import Thunk from './components/pages/ThunkDemo/Thunk';
import ApeRoutings from './components/pages/Ape/routes';


const OrdererMain = React.lazy(() => import('./components/pages/Orderer'))
function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route index element={<Home />} />
        <Route path="page1" element={<Page1 />} />
        <Route path="page2" element={<Page2 />} />
        <Route path="page2/:id/:fancy" element={<Page2S />} />
        {/* <Route path="orderer" element={<OrdererMain />} /> */}
        <Route path="orderer" element={
          // <Thunk />
          <React.Suspense fallback={<>...</>} >
            <OrdererMain />
          </React.Suspense>
        }>
        </Route>

        <Route path="ape">
          {/* {ApeRoutings()} */}
          <Route index element={<ApeList />} />
          <Route path=":username" element={<Ape300 />} />
        </Route>
        {/* <Route path="funky" element={
          // <Thunk />
          <React.Suspense fallback={<>...</>} >
            <Thunk />
          </React.Suspense>
        } 
        >
        </Route>*/}
        <Route path="*" element={<span>404</span>} />
      </Routes>


      <nav>
        <ul>
          <li>
            <Link to={'/page1'}>страница 1</Link>
          </li>
          <li>
            <Link to={'/page2'}>страница 2</Link>
          </li>
          <li>
            <Link to={'/orderer'}>Цветные кнопки</Link>
          </li>
          <li>
            <Link to={'/ape'}>Пользователи</Link>
          </li>
          {/* <li>
            <Link to={'/funky'}>Thunk</Link>
          </li> */}
        </ul>
      </nav>





    </BrowserRouter >
  );
}

export default App;
